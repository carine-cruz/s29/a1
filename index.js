//mock database

const express = require("express");
const app = express(); //createServer
const PORT = 3008;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//solution1
app.get("/home", (req, res) => res.status(200).send(`Welcome to Homepage!`))

//solution3
let users = [
    {"userName": "user1", "password": "1234"},
    {"userName": "user2", "password": "1234"},
    {"userName": "user3", "password": "1234"}
] 
app.get("/users", (req, res) => {
    res.status(200).send(users);
})

//solution5
app.delete("/delete-user", (req, res) => {
    const {userName, password} = req.body;
    let message="";

    for(let i = 0; i < users.length; i++){
		if(users[i].userName === userName){
			let delUser = users.splice([i],1);
			message = `User ${delUser[0].userName} has been deleted.`;
            console.log(delUser);
			break;
		} else {
			message = `User does not exist.`;
		}
	}
    console.log(users);
    res.status(200).send(message);
})

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`));